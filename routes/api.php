<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// TODO: api middleware removed in demo purposes, put it back if auth is needed.
Route::group(['prefix' => 'v1'], function() {

    // --- Products
    Route::resource('products', 'ProductController', ['only' => [
        'index', 'store', 'show', 'update', 'destroy'
    ]]);

    // Associate voucher to product
    Route::post('products/{productId}/associate-voucher/{voucherId}', 'ProductController@associateVoucher');

    // Disassociate voucher from product
    Route::post('products/{productId}/disassociate-voucher/{voucherId}', 'ProductController@disassociateVoucher');

    // Purchase a product
    Route::post('products/{productId}/purchase', 'ProductController@purchase');

    // --- Vouchers
    Route::resource('vouchers', 'VoucherController', ['only' => [
        'index', 'store', 'show', 'update', 'destroy'
    ]]);

    // Associate discount tire to voucher
    Route::post('vouchers/{voucherId}/associate-discount/{discountTireId}', 'VoucherController@associateDiscountTire');

    // Disassociate discount tire from voucher
    Route::post('vouchers/{voucherId}/disassociate-discount', 'VoucherController@disassociateDiscountTire');

    // --- DiscountTires
    Route::resource('discount-tires', 'DiscountTireController', ['only' => [
        'index', 'store', 'show', 'update', 'destroy'
    ]]);
});
