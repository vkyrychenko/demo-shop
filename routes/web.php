<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// TODO: move web routes to frontend - react - redux or vue.js or angular and use REST API routes
Route::get('/', function () {
    return view('welcome', ['products' => \App\Product::all()]);
});

Route::get('/vouchers', function () {
    return view('vouchers', ['vouchers' => \App\Voucher::all()]);
});