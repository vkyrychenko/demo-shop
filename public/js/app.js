'use strict';

$(function(){
    $('.js-buy-it').on('click', function(){

        hideError();

        var $button = $(this),
            productId = $button.data('product');

        if(!productId){
            return console.log("Error: there is no product ID");
        }

        $.post('/api/v1/products/' + productId + '/purchase', function(data){

            if(typeof data.status !== 'undefined' && data.status == 'ok'){
                $button.closest('tr').find('.js-available').css({color: 'red'}).text('No');
                $button.replaceWith('<span class="glyphicon glyphicon-ok-circle" style="font-size: 35px; text-align: center; width: 60px;color: rgba(38, 136, 29, 0.79);" aria-hidden="true"></span>');
            }

            if(typeof data.error !== 'undefined' && data.error){
                showError(data.error);
            }
        });

    });

    function showError(message) {
        $('.js-error').css({display: 'block'});

        if($('.js-error').length <= 0) {
            $('.panel').before('<div class="alert alert-danger js-error" role="alert">' + message + '</div>');
        }
    }

    function hideError() {
        $('.js-error').css({display: 'none'});
    }

});