## Installation

1) Go to directory where you want to install application.

2) Clone repository
```
#!
git clone git@bitbucket.org:vkyrychenko/demo-shop.git
```
3) Create database using your favorite MySQL client with SQL command:
```
#!
CREATE DATABASE `demo-shop` CHARACTER SET utf8 COLLATE utf8_general_ci;

```
4) Go to project root and copy .env.example file to .env
```
#!
cd demo-shop

cp .env.example .env

```
5) Change database connection parameters. They starts with prefix 'DB_'.
```
#!

DB_DATABASE=demo-shop # <- we already create database with this name, can keep it
DB_USERNAME=homestead # <- change to your database user name
DB_PASSWORD=secret    # <- change to your database password
```
6) If you don't have composer installed, please [install it.](https://getcomposer.org/)

7) Install composer dependencies by running:
```
#!
composer update
```
8) Generate database tables by running migration:
```
#!
php artisan migrate
```
9) Seed database with random data;
```
#!
php artisan db:seed
```
10) For demo it's enough to start built in server instead of configuring apache or nginx. Run from project root:
```
#!
php artisan serve
```




## Testing API
In our demo case, and dead line we'll use [POSTMAN](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) tool for making requests to API instead of Swagger.

After installation of POSTMAN import API collection from 'demo-shop.postman_collection.json' file in project root. After importing all available API routes will be shown in GUI of POSTMAN.