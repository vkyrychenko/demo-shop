<?php

namespace App\Criterias;


abstract class Criteria {

    /**
     * @param $model
     * @return mixed
     */
    public abstract function apply($model);
}