<?php


namespace App\Criterias;


class OnlyAvailableCriteria extends Criteria
{

    /**
     * @param $model
     * @return mixed
     */
    public function apply($model)
    {
        if(empty($model)){
            return null;
        }

        if(is_object($model)){
            return $model->where('available', '=', 1);
        }

        if(class_exists($model)){
            return $model::where('available', '=', 1);
        }

        return null;
    }
}