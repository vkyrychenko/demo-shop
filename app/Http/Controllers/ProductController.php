<?php

namespace App\Http\Controllers;

use App\Criterias\OnlyAvailableCriteria;
use App\Exceptions\ProductPurchasedException;
use App\Product;
use App\Voucher;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends RestApiResourceController
{
    /**
     * Define model class that used as resource for REST api.
     *
     * @var null|string
     */
    protected $modelClass = Product::class;

    /**
     * Criteria class to fetch available resources only.
     * @var null|string
     */
    protected $onlyAvailableCriteriaClass = OnlyAvailableCriteria::class;

    /**
     * Associate voucher to product.
     *
     * @param $productId
     * @param $voucherId
     * @return \Illuminate\Http\JsonResponse
     */
    public function associateVoucher($productId, $voucherId)
    {

        try {

            /** @var Product $product */
            $product = Product::findOrFail($productId);

            /** @var Voucher $voucher */
            $voucher = Voucher::findOrFail($voucherId);

            // Create relation if it's not exist
            if(!$product->vouchers()->find($voucherId)){
                $product->vouchers()->save($voucher);
            }

            return response()->json(['status' => 'ok']);

        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 404);

        } catch (\Exception $e) {

            $this->handleSystemException($e);

        }

    }

    /**
     * Disassociate voucher from product.
     *
     * @param $productId
     * @param $voucherId
     * @return \Illuminate\Http\JsonResponse
     */
    public function disassociateVoucher($productId, $voucherId)
    {

        try {

            /** @var Product $product */
            $product = Product::findOrFail($productId);

            /** @var Voucher $voucher */
            $voucher = Voucher::findOrFail($voucherId);

            // Create relation if it's not exist
            if($product->vouchers()->find($voucherId)){
                $product->vouchers()->detach([$voucher->id]);
            }

            return response()->json(['status' => 'ok']);


        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 404);

        } catch (\Exception $e) {

            $this->handleSystemException($e);

        }
    }

    /**
     * Purchase product.
     *
     * @param $productId
     * @return \Illuminate\Http\JsonResponse
     */
    public function purchase($productId)
    {

        try {

            /** @var Product $product */
            $product = Product::findOrFail($productId);
            $purchasedPrice = $product->purchase();

            return response()->json([
                'status' => 'ok',
                'price'  => $purchasedPrice
            ]);

        } catch (ProductPurchasedException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 200);

        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 404);

        } catch (\Exception $e) {

            $this->handleSystemException($e);

        }
    }


    /**
     * Get validation rules.
     * @return array
     */
    protected function getValidationRules()
    {
        return [
            'name' => 'required|max:255|min:1',
            'price' => 'required',
        ];
    }
}
