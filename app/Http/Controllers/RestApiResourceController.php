<?php


namespace App\Http\Controllers;
use App\Criterias\Criteria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Base class for REST API Controllers.
 *
 * Class RestApiResourceController
 * @package App\Http\Controllers
 */
class RestApiResourceController extends Controller
{
    /**
     * Define model class that used as resource for REST api.
     *
     * @var null|string
     */
    protected $modelClass = null;

    /**
     * @var null|string
     */
    private $modelName = null;

    /**
     * Criteria to fetch available resources only.
     * @var null|Criteria
     */
    private $onlyAvailableCriteria = null;

    /**
     * Criteria class to fetch available resources only.
     * @var null|string
     */
    protected $onlyAvailableCriteriaClass = null;

    public function __construct()
    {
        // Check if Model class exists
        if(empty($this->modelClass) || !class_exists($this->modelClass)){
            throw new \Exception("Model class not exist or empty!");
        }

        // Check if Criteria class exists
        if(!empty($this->onlyAvailableCriteriaClass) && class_exists($this->onlyAvailableCriteriaClass)){
            $this->onlyAvailableCriteria = new $this->onlyAvailableCriteriaClass();
        }

        $this->modelName = Str::lower(class_basename($this->modelClass));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(empty($this->onlyAvailableCriteria)){
            return response()->json($this->modelClass::orderBy('id', 'desc')->get());
        }

        $query = $this->onlyAvailableCriteria->apply($this->modelClass);
        return (!empty($query))
            ? response()->json($query->orderBy('id', 'desc')->get())
            : response()->json([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            $this->validate($request, $this->getValidationRules());

            /** @var Model $model */
            $model = new $this->modelClass($request->all());
            $model->saveOrFail();

            return response()->json([
                'status' => 'ok',
                $this->modelName => $model
            ]);

        } catch (ValidationException $e){

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage(),
                'validation_errors' => $e->validator->failed()
            ]);

        } catch (\Exception $e) {

            return $this->handleSystemException($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resourceId integer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($resourceId)
    {

        try {

            if(empty($this->onlyAvailableCriteria)) {
                return response()->json($this->modelClass::findOrFail($resourceId));
            }

            $query = $this->onlyAvailableCriteria->apply($this->modelClass);

            if(empty($query)){
                return response()->json([]);
            }

            return response()->json($query->where('id', $resourceId)->first());

        } catch (ModelNotFoundException $e) {

            return $this->handleNotFoundException($e, $resourceId);

        } catch (\Exception $e) {

            return $this->handleSystemException($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $resourceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $resourceId)
    {
        try {

            $this->validate($request, $this->getValidationRules());

            /** @var Model $model */
            $model = $this->modelClass::findOrFail($resourceId);
            $model->fill($request->all());
            $model->save();

            return response()->json([
                'status' => 'ok',
                $this->modelName => $model
            ]);

        } catch (ValidationException $e){

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage(),
                'validation_errors' => $e->validator->failed()
            ]);

        } catch (ModelNotFoundException $e) {

            return $this->handleNotFoundException($e, $resourceId);

        } catch (\Exception $e) {

            return $this->handleSystemException($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $resourceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($resourceId)
    {

        try {

            $model = $this->modelClass::findOrFail($resourceId);

            return ($model->delete())
                ? response()->json(['status' => 'ok'])
                : response()->json(['status' => 'error', 'error' => 'Can\'t delete']);

        } catch (ModelNotFoundException $e) {

            return $this->handleNotFoundException($e, $resourceId);

        } catch (\Exception $e) {

            return $this->handleSystemException($e);

        }
    }

    /**
     * Get validation rules.
     * @return array
     */
    protected function getValidationRules()
    {
        return [];
    }

    /**
     * Handle ModelNotFoundException.
     *
     * @param \Exception $e
     * @param $resourceId
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleNotFoundException(\Exception $e, $resourceId)
    {
        return response()->json([
            'status' => 'error',
            'error'  => "Resource [{$this->modelName}] with ID: {$resourceId} not found."
        ], 404);
    }

    /**
     * Handle Exception.
     *
     * @param \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function handleSystemException(\Exception $e)
    {
        Log::error(sprintf("%s -> %s", static::class, $e->getMessage()));

        return response()->json([
            'status' => 'error',
            'error'  => 'System error appear, please contact support'
        ]);
    }
}