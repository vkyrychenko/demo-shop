<?php

namespace App\Http\Controllers;

use App\DiscountTire;

class DiscountTireController extends RestApiResourceController
{

    /**
     * Define model class that used as resource for REST api.
     *
     * @var null|string
     */
    protected $modelClass = DiscountTire::class;

    /**
     * Get validation rules.
     * @return array
     */
    protected function getValidationRules()
    {
        return [
            'name'      => 'required|min:1|max:255',
            'discount'  => 'required|integer|max:99',
        ];
    }
}
