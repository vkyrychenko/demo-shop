<?php

namespace App\Http\Controllers;

use App\Criterias\OnlyAvailableCriteria;
use App\DiscountTire;
use App\Voucher;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VoucherController extends RestApiResourceController
{

    /**
     * Define model class that used as resource for REST api.
     *
     * @var null|string
     */
    protected $modelClass = Voucher::class;

    /**
     * Criteria class to fetch available resources only.
     * @var null|string
     */
    protected $onlyAvailableCriteriaClass = OnlyAvailableCriteria::class;

    /**
     * Associate discount to voucher.
     *
     * @param $voucherId
     * @param $discountTireId
     * @return \Illuminate\Http\JsonResponse
     * @internal param $productId
     */
    public function associateDiscountTire($voucherId, $discountTireId)
    {

        try {

            /** @var Voucher $voucher */
            $voucher = Voucher::findOrFail($voucherId);

            /** @var DiscountTire $discountTire */
            $discountTire = DiscountTire::findOrFail($discountTireId);

            // Create relation if it's not exist
            if($voucher->discount_tire_id !== $discountTireId){
                $voucher->discount_tire_id = $discountTireId;
                $voucher->save();
            }

            // Attach associated discount to voucher response
            $voucher->discountTire = $discountTire;

            return response()->json([
                'status' => 'ok',
                'voucher' => $voucher
            ]);

        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 404);

        } catch (\Exception $e) {

            $this->handleSystemException($e);
        }

    }

    /**
     * Disassociate voucher from product.
     *
     * @param $voucherId
     * @return \Illuminate\Http\JsonResponse
     */
    public function disassociateDiscountTire($voucherId)
    {
        try {

            /** @var Voucher $voucher */
            $voucher = Voucher::findOrFail($voucherId);

            $voucher->discount_tire_id = null;
            $voucher->save();

            return response()->json([
                'status' => 'ok',
                'voucher' => $voucher
            ]);

        } catch (ModelNotFoundException $e) {

            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ], 404);

        } catch (\Exception $e) {

            $this->handleSystemException($e);
        }
    }


    /**
     * Get validation rules.
     * @return array
     */
    protected function getValidationRules()
    {
        return [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d',
        ];
    }
}
