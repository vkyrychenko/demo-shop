<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Voucher extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'end_date', 'available'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get Products which associated to Voucher.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_vouchers', 'voucher_id', 'product_id')
            ->withTimestamps();
    }

    /**
     * Get DiscountTire associated with this Voucher.
     */
    public function discountTire()
    {
        return $this->hasMany(DiscountTire::class, 'id', 'discount_tire_id');
    }
}
