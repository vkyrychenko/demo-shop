<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DiscountTire extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'discount', 'discount_tire_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get Vouchers which associated to DiscountTire
     */
    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }
}
