<?php

namespace App;

use App\Exceptions\ProductPurchasedException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'price', 'available'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get associated Vouchers.
     */
    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'products_vouchers', 'product_id', 'voucher_id')
            ->withTimestamps();
    }

    /**
     * Calculate price that should be payed for the product.
     */
    public function calcPriceForPurchase()
    {
        // TODO:
        // on live environment is better to move
        // domain logic to separate layer and use bc_math extension of php
        // for Math operations.

        /** @var Collection $vouchers */
        $vouchers = $this->vouchers()->with('discountTire')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->get();

        // If no vouchers - return product price
        if(empty($vouchers) || $vouchers->count() == 0){
            return $this->price;
        }

        $discount = 0;

        // Calculate vouchers discount
        foreach ($vouchers as $voucher){

            /** @var Voucher $voucher */
            if(empty($voucher->discountTire) || $voucher->discountTire->count() == 0){
                continue;
            }

            // TODO: use bcadd(), @see - http://php.net/manual/ru/function.bcadd.php
            $discount += $voucher->discountTire->first()->discount;
        }

        // If there is no discounts - return product price
        if(empty($discount)){
            return $this->price;
        }

        // If discount more then 60% use 60% of products price
        if($discount >= 60){

            // TODO use bcmath functions
            return $this->price * 0.6;
        }

        // TODO use bcmath functions
        return number_format($this->price * ($discount / 100), 2);
    }

    /**
     * Purchase product.
     */
    public function purchase()
    {

        if($this->available == 0){
            throw new ProductPurchasedException("Product is not available");
        }

        $price = $this->calcPriceForPurchase();

        // Make not available product
        $this->update(['available' => 0]);
        $vouchers = $this->vouchers()->get();

        if(empty($vouchers) || $vouchers->count() == 0){
            return $price;
        }

        // Make not available vouchers
        foreach ($vouchers as $voucher){
            if(empty($voucher)){
                continue;
            }

            $voucher->update(['available' => 0]);
        }

        return $price;
    }
}
