@extends('layout')

@section('title', 'Products')

@section('content')


    @if(!empty($products) && count($products) > 0)
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Panel heading</div>

            <!-- Table -->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Available</th>
                        <th>Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $i => $product)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$product->name}}</td>
                            <td>
                                @if(($discountPrice = $product->calcPriceForPurchase()) < $product->price)
                                    <span style="text-decoration: line-through; color: rgba(0, 39, 255, 0.6);">{{$product->price}}$</span>
                                    <br><span style="color: rgba(65, 118, 60, 0.79);">{{$discountPrice}}$</span>
                                @else
                                    <span style="color: rgba(65, 118, 60, 0.79);">{{$product->price}}$</span>
                                @endif
                            </td>
                            <td>
                                <span class="js-available" style="color: {{($product->available == 1) ? 'rgba(65, 118, 60, 0.79)' : 'red'}};">
                                    {{($product->available == 1) ? 'Yes' : 'No'}}
                                </span>
                            </td>
                            <td>{{$product->created_at}}</td>
                            <td>
                                <button type="button" class="btn btn-primary js-buy-it"
                                        data-product="{{$product->id}}">
                                    Buy it
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    @endif

@endsection