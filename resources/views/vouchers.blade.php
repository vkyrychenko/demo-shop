@extends('layout')

@section('title', 'Vouchers')

@section('content')
    @if(!empty($vouchers) && count($vouchers) > 0)
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Panel heading</div>

            <!-- Table -->
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Discount name</th>
                    <th>Discount</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Available</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vouchers as $i => $voucher)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$voucher->discountTire->first()->name}}</td>
                        <td>{{$voucher->discountTire->first()->discount}}%</td>
                        <td>{{$voucher->start_date}}</td>
                        <td>{{$voucher->end_date}}</td>
                        <td>{{($voucher->available == 1) ? 'Yes' : 'No'}}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    @endif

@endsection