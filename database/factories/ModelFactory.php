<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

// Product factory
$factory->define(App\Product::class, function (Faker\Generator $faker) {

    return [
        'name'        => $faker->name,
        'price'       => $faker->randomNumber(2),
        'description' => $faker->text,
        'available'   => 1
    ];
});

// Voucher factory
$factory->define(App\Voucher::class, function (Faker\Generator $faker) {

    return [
        'start_date' => $faker->date(),
        'end_date'   => $faker->date(),
        'available'  => 1
    ];
});

// DiscountTire factory
$factory->define(App\DiscountTire::class, function (Faker\Generator $faker) {

    $availableDiscounts = [10, 15, 20, 25];

    return [
        'name'      => sprintf("Tire - %s", $faker->name),
        'discount'  => $availableDiscounts[array_rand($availableDiscounts, 1)],
    ];
});
