<?php

use App\DiscountTire;
use App\Voucher;
use Illuminate\Database\Seeder;

class VouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discountTires = [];
        $availableDiscountTires = [10, 15, 20, 25];

        // Generate available discounts if not exist
        foreach ($availableDiscountTires as $discount){

            if($discountTire = DiscountTire::where('discount', $discount)->first()){
                $discountTires[] = $discountTire;
                continue;
            }

            $discountTires[] = factory(DiscountTire::class)->create(['discount' => $discount]);
        }

        factory(Voucher::class, 10)->create()->each(function($voucher) use ($discountTires){

            // Pick random one from discounts
            $discountTire  = $discountTires[array_rand($discountTires, 1)];

            /** @var $voucher \App\Voucher */
            $voucher->discount_tire_id = $discountTire->id;
            $voucher->save();
        });
    }
}
